let toDoList = [];
let completedList = [];

const getEle = (id) => document.getElementById(id);

const saveToDoListLocalStorage = (data) => {
  let dataJSON = JSON.stringify(data);
  localStorage.setItem("toDoList", dataJSON);
};

const getToDoListFromLocalStorage = () => {
  let dataJSON = JSON.parse(localStorage.getItem("toDoList"));
  if (!dataJSON) return;
  toDoList = dataJSON;
};

const saveCompletedListLocalStorage = (data) => {
  let dataJSON = JSON.stringify(data);
  localStorage.setItem("completedList", dataJSON);
};

const getCompletedListFromLocalStorage = () => {
  let dataJSON = JSON.parse(localStorage.getItem("completedList"));
  if (!dataJSON) return;
  completedList = dataJSON;
};

getToDoListFromLocalStorage();
getCompletedListFromLocalStorage();

const renderToDoList = (arr) => {
  let toDoListHTML = "";
  arr.forEach((item, index) => {
    toDoListHTML += `
    <li>${item}
    <div>
        <i onclick="handleDeleteTask(${index})" class="fa fa-trash-alt"></i>
        <i onclick="handleCompletedTask(${index})" class="fa fa-check-circle"></i>
     </div>
   </li>
        `;
  });
  getEle("todo").innerHTML = toDoListHTML;
};
renderToDoList(toDoList);

const renderCompletedList = (arr) => {
  let completedHTML = "";
  arr.forEach((item, index) => {
    completedHTML += `
    <li style="font-weight:500; color: #43A78F">${item}
     <div>
         <i onclick="handleDeleteCompleted(${index})" class="fa fa-trash-alt"></i>
         <i style="color: #43A78F" onclick="handleRemoveCompleted(${index})" class="fa fa-check-circle"></i>
      </div>
    </li>
    `;
  });
  getEle("completed").innerHTML = completedHTML;
};
renderCompletedList(completedList);

const renderAndSave = (toDoList, completedList) => {
  renderToDoList(toDoList);
  renderCompletedList(completedList);
  saveToDoListLocalStorage(toDoList);
  saveCompletedListLocalStorage(completedList);
};

const handleAddTasks = () => {
  let task = getEle("newTask").value;
  toDoList.push(task);

  getEle("newTask").value = "";

  renderAndSave(toDoList, null);
};
window.handleAddTasks = handleAddTasks;

const handleDeleteTask = (index) => {
  toDoList.splice(index, 1);

  renderAndSave(toDoList, completedList);
};
window.handleDeleteTask = handleDeleteTask;

const handleDeleteCompleted = (index) => {
  completedList.splice(index, 1);

  renderAndSave(toDoList, completedList);
};
window.handleDeleteCompleted = handleDeleteCompleted;

const handleCompletedTask = (index) => {
  completedList.push(toDoList[index]);
  toDoList.splice(index, 1);

  renderAndSave(toDoList, completedList);
};
window.handleCompletedTask = handleCompletedTask;

const handleRemoveCompleted = (index) => {
  toDoList.push(completedList[index]);
  completedList.splice(index, 1);

  renderAndSave(toDoList, completedList);
};
window.handleRemoveCompleted = handleRemoveCompleted;

const sortASC = () => {
  toDoList.sort();
  completedList.sort();

  renderAndSave(toDoList, completedList);
};
window.sortASC = sortASC;

const sortDES = () => {
  toDoList.sort();
  toDoList.reverse();
  completedList.sort();
  completedList.reverse();

  renderAndSave(toDoList, completedList);
};
window.sortDES = sortDES;

const sortChecked = () => {
  toDoList.forEach((item) => {
    completedList.push(item);
  });
  toDoList = [];

  renderAndSave(toDoList, completedList);
};
window.sortChecked = sortChecked;

const sortPending = () => {
  completedList.forEach((item) => {
    toDoList.push(item);
  });
  completedList = [];

  renderAndSave(toDoList, completedList);
};
window.sortPending = sortPending;
